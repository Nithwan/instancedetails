import boto3
import pytest
import json
from moto import mock_ec2
from InstanceDetails.src.instance_details import *

@pytest.fixture
def use_moto():
    @mock_ec2
    def ec2_client():
        client = boto3.client('ec2')
        group_instances = client.describe_instances()['Reservations'] 
        return group_instances
    return ec2_client

@mock_ec2
def test_handler_for_failure(use_moto):
    use_moto()
    event = {}
    return_data = handler(event, "")
    assert return_data['statusCode'] == 200
    
@mock_ec2
def test_handler_for_status_ok(use_moto):
    use_moto()
    event = {}
    return_data = handler(event, "")
    body = json.loads(return_data['body'])
    assert return_data['statusCode'] == 200