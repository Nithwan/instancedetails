LAMBDA=$1
ENV=$2
region=$3
qualifiedLambdaName="$1"

echo "Pulling env variables from $qualifiedLambdaName"

CURRENTVARIABLES=$(aws lambda get-function-configuration --function-name $qualifiedLambdaName --region $region| jq '.Environment.Variables')
NEWVARIABLES=$(echo $CURRENTVARIABLES | jq '. += {"PYTHON_ENV":"'$ENV'"}')
COMMAND="aws lambda update-function-configuration --function-name $LAMBDA --region $region --environment '{\"Variables\":$NEWVARIABLES}'"
eval $COMMAND
